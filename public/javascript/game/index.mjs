import { addClass } from '../helpers/domHelper.mjs'
import { removeClass } from '../helpers/domHelper.mjs'
import { showMessageModal } from '../views/modal.mjs'
import { setProgress } from '../views/user.mjs'

const readyBtn = document.querySelector('#ready-btn')
const GameTimer = document.querySelector('#game-timer-seconds')
const WordTimer = document.querySelector('#timer')

const text = document.querySelector('#text-container')
const gameTimer = document.querySelector('#game-timer')

let gameTime = 0
let wordTime = 0
let constWordTime = 0
let data = []
let isPlaying
let score = 0
let username = ''
let currentUserWord = ''
let countdownInterval
let checkStatusInterval

export const playGame = (_username, _gameTime, _wordTime, _text) => {
  constWordTime = _wordTime
  gameTime = _gameTime
  wordTime = _wordTime
  data = _text
  username = _username

  showGame()

  showWord(data)

  window.addEventListener('keyup', (e) => {
    currentUserWord += e.key
    startMatch()
  })

  countdownInterval = setInterval(countdown, 1000)

  checkStatusInterval = setInterval(checkStatus, 50)
}

const startMatch = () => {
  if (matchWords()) {
    isPlaying = true
    wordTime = constWordTime
    currentUserWord = ''
    showWord(data)
    score+=30
    if(score >= 100) {
        isPlaying = false
        showMessageModal({
            message: `Winner is: ${username}`,
            onClose: () => {},
          })

          gameTime = 0
        
        return
        
    }
    setProgress({ username, progress: score })

    
  }
}

const matchWords = () => {
  if (currentUserWord === text.innerText) {
    console.log('true')
    return true
  } else {
    console.log('false')
    return false
  }
}

const showWord = (words) => {
  const randomIndex = Math.floor(Math.random() * words.length)
  text.innerText = words[randomIndex]
}

const countdown = () => {
  if (gameTime > 0) {
    wordTime--
    if (wordTime === 0) {
      showWord(data)
      wordTime = constWordTime
      currentUserWord = ''
    }
    gameTime--
  } else if (gameTime === 0) {
    isPlaying = false
  }

  GameTimer.innerText = gameTime
  WordTimer.innerText = wordTime
}

const checkStatus = () => {
  if (!isPlaying && gameTime === 0) {
    showMessageModal({
      message: `Score: ${score}`,
      onClose: () => {},
    })
    addClass(GameTimer, 'display-none')
    addClass(WordTimer, 'display-none')

    addClass(text, 'display-none')
    addClass(gameTimer, 'display-none')

    removeClass(readyBtn, 'display-none')
    setProgress({ username, progress: 0 })

    isPlaying = false
    clearInterval(countdownInterval)
    clearInterval(checkStatusInterval)
  }
}

const showGame = () => {
  readyBtn.addEventListener('click', () => {
    removeClass(GameTimer, 'display-none')
    removeClass(WordTimer, 'display-none')

    removeClass(text, 'display-none')
    removeClass(gameTimer, 'display-none')

    addClass(readyBtn, 'display-none')
  })

  GameTimer.innerHTML = gameTime
  WordTimer.innerText = wordTime
}
