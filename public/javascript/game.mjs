import { playGame } from './game/index.mjs'
import { addClass } from './helpers/domHelper.mjs'
import { removeClass } from './helpers/domHelper.mjs'
import { showInputModal } from './views/modal.mjs'
import { appendRoomElement } from './views/room.mjs'
import { appendUserElement } from './views/user.mjs'

const username = sessionStorage.getItem('username')
// Globals
let wordTime = 10
let gameTime
let data = []
let rooms = []

if (!username) {
  window.location.replace('/login')
}

const socket = io('', { query: { username } })

const User = {
  name: username,
  room: '',
}

socket.on('textData', texts => {
  data = [...texts]
})

socket.on('gameSettings', ({maxUsers,timerBeforeStart,gameSeconds})=> {
  gameTime = gameSeconds
})

socket.on('showRooms', (rooms)=> {
  console.log('rooms',rooms);
})

socket.on('showUser', (message) => {
  if (User.name === message) {
    appendUserElement({
      username: User.name,
      ready: false,
      isCurrentUser: true,
    })
  } else {
    appendUserElement({
      username: message,
      ready: false,
      isCurrentUser: false,
    })
  }
})

socket.on('showRoom', ({ name, numberOfUsers }) => {
  if (name) {
    rooms.push(name)
    socket.emit('newRoom', name)

    appendRoomElement({
      name: name,
      numberOfUsers: numberOfUsers,
      onJoin: () => {
        const gamePage = document.querySelector('#game-page')
        const roomsPage = document.querySelector('#rooms-page')
        addClass(roomsPage, 'display-none')
        removeClass(gamePage, 'display-none')
        socket.emit('userNameMsg', User.name)
        ++numberOfUsers
        playGame(username, gameTime, wordTime, data)
      },
    })
  }
})

document.querySelector('#add-room-btn').addEventListener('click', () => {
  showInputModal({
    title: 'Create room',
    onChange: (e) => {
      User.room = e
    },
    onSubmit: () => {
      socket.emit('roomNameMsg', User.room)
    },
  })


  
})