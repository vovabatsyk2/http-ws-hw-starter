import { Server } from 'socket.io'
import { texts } from '../data'
import { formatRoomMessage, formatRoomName } from '../utils/messages'
import * as config from './config'

export default (io: Server) => {
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username

    socket.on('roomNameMsg', (msg) => {
      io.emit('showRoom', formatRoomMessage(msg, 0))
    })

	socket.on('userNameMsg', (msg) => {		
		io.emit('showUser', msg)
	  })

	  socket.on('newRoom', (name) => {		
		  debugger
		io.emit('showRooms', formatRoomName(name, 0))
	  })

	io.emit('gameSettings', {
		maxUsers: config.MAXIMUM_USERS_FOR_ONE_ROOM,
		timerBeforeStart:config.SECONDS_TIMER_BEFORE_START_GAME,
		gameSeconds:config.SECONDS_FOR_GAME
	})
	io.emit('textData', texts as Array<string>)
  })

}
