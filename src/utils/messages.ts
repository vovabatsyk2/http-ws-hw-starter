import { IRoom } from "../interfaces/IRoom"

export const formatRoomMessage = (name: string, numberOfUsers: number): IRoom => {
  return { name, numberOfUsers }
}

export const formatRoomName = (name: string, numberOfUsers: number): IRoom[] => {
  const rooms:IRoom[] = []
  rooms.push({name, numberOfUsers})
  return rooms
}
