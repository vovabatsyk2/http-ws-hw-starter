export interface IRoom {
    name: string,
    numberOfUsers: number
}